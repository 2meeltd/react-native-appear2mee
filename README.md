
# react-native-appear2mee

## Getting started

`$ npm install react-native-appear2mee --save`

### Mostly automatic installation

`$ react-native link react-native-appear2mee`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-appear2mee` and add `RNReactNativeAppear2mee.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNReactNativeAppear2mee.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.twomee.react.ReactNativeAppear2meePackage;` to the imports at the top of the file
  - Add `new ReactNativeAppear2meePackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-appear2mee'
  	project(':react-native-appear2mee').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-appear2mee/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      implement project(':react-native-appear2mee')
  	```


## Usage
```javascript
import RNAppear2mee from 'react-native-appear2mee';

// TODO: What to do with the module?
RNAppear2mee;
```
  
