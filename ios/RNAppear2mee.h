/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import <React/RCTEventEmitter.h>
#import <React/RCTBridgeModule.h>
#if !TARGET_OS_TV
@import UserNotifications;
#endif
@class UNNotificationResponse;

extern NSString * _Nonnull const kAppear2meeResponseDismiss;
extern NSString * _Nonnull const kAppear2meeResponseAccept;

typedef void (^RCTRemoteNotificationCallback)(UIBackgroundFetchResult result);

@interface RNAppear2mee : RCTEventEmitter <RCTBridgeModule>

/**
 *  Initialise the Appear2mee connection.
 *
 *  @param  appear2meeID ID supplied by the 2mee Exchange
 *  @param  appear2meeKey Key supplied by the 2mee Exchange
 *  @param  appgroup The common app group eg., group.com.2mee.Appear2mee, used to share files.
 */
+ (void)initWithID:(NSString* _Nonnull) appear2meeID  clientKey:(NSString* _Nonnull)appear2meeKey  usingAppGroup: (NSString * _Nonnull) appgroup;

/**
 *  Registers the deviceToken to allow Appear2mee to receive remote notifications from the 2mee Exchange server.
 *  Should be used in the (UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken: call of your Application Delegate.
 *
 *  @param  deviceToken The deviceToken supplied in the didRegisterForRemoteNotificationsWithDeviceToken:
 *  @return The device 2meeID as an NSString.
 */
+ (NSString * _Nonnull)registerDeviceToken:(NSData* _Nonnull)deviceToken;

/**
 * Adds a UNNotificationCategory for the Appear2mee content extension actions to the existing set of UNUserNotificationCenter NotificationCategories.
 *
 * Should only be used in the UNUserNotificationCenter requestAuthorizationWithOptions:options
 completionHandler: method,
 *
 * Note add and other UINotificationCategory first as this method adds to the existing set. If you replace the set later then the Appear2mee  UNNotificationCategory will be lost.
 * Only use this method if you are providing the Appear2mee Notification Extension in your app.
 *
 */
+(void) addAppear2meeNotificationCategory;

/**
* Handle a notification response. If response is from content extension then will the appropriate action function. If default responses will either call the dismiss action block or display a dialog using the contents of a UNNotificationResponse. Allows for text/image/animation/video/face message display.
*
* Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
*
* @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
* @param acceptAction a void (^)(NSDictionary *) block to be executed if the "accept/OK" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined but the 2mee Exchange.
* @param dismissAction a void (^)(NSDictionary *) block to be executed if the "Dismiss" button is pressed on the notification dialog. The NSDictionary block argument contains the Key/Values sent in the notification, defined but the 2mee Exchange.
*/

+(BOOL) didReceiveNotificationResponse:(UNNotificationResponse *_Nonnull)response withAcceptAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) acceptAction withDismissAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) dismissAction API_AVAILABLE(ios(10.0));


/**
 *  Forwards the push notification to Appear2mee. If the notification payload is not recognised as a 2mee message the function returns false, leaving the app to deal with it. A 2mee message is processed and the media is downloaded. Currently the media can be stored awaiting another push alert.
 
 This function should be called in the application delegate application:didReceiveRemoteNotification:fetchCompletionHandler: method.
 
 Important: The application must have the appropriate setting in its Capablities to allow background fetch push notifications.
 
 @param payload The payload dictionary from the application:didReceiveRemoteNotification:fetchCompletionHandler: method args.
 @param completionHandler The callback block from the application:didReceiveRemoteNotification:fetchCompletionHandler: method args.
 */
+(BOOL) didReceiveRemoteNotification:(NSDictionary * _Nonnull) payload fetchCompletionHandler:(void (^_Nullable)(UIBackgroundFetchResult result))completionHandler;


/**
 *  Returns true if the UNNotificationResponse response is from an Appear2mee notification else false.
 *
 * Called only in the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method
 *
 * @param response A UNNotificationResponse received in  the userNotificationCenter:didReceiveNotificationResponse:withCompletionHandler: method.
 *    @return true if an Appear2mee notification else false.
 */
+(BOOL) isAppear2meeNotificationResponse:(UNNotificationResponse *_Nullable)response;

/**
 Checks if a (UNNotification *)notification is a notification originating from the 2mee Exchange
 
 @param notification The notification
 @returns True, if a 2mee Exchange notification, else false.
 */
+ (BOOL) isAppear2meeNotification:(UNNotification *_Nullable)notification;


/**
 Checks if a (NSDictionary *) userInfo is a userInfo originating from the 2mee Exchange
 
 @param userInfo The dictionary to test.
 @returns True, if a 2mee Exchange notification, else false.
 */
+ (BOOL) isAppear2meeUserInfo:(NSDictionary *_Nullable)notification;

#if !TARGET_OS_TV
+ (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
//+ (void)didReceiveRemoteNotification:(NSDictionary *)notification;
//+ (void)didReceiveRemoteNotification:(NSDictionary *)notification fetchCompletionHandler:(RCTRemoteNotificationCallback)completionHandler;
+ (void)didReceiveLocalNotification:(UILocalNotification *)notification;
+ (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler;
#endif




@end
