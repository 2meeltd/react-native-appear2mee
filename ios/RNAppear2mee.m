/**
* Copyright (c) Facebook, Inc. and its affiliates.
*
* This source code is licensed under the MIT license found in the
* LICENSE file in the root directory of this source tree.

  Used as template for 2mee Ltd Copyright code ... huge changes.
*/

#import "RNAppear2mee.h"

#import <UserNotifications/UserNotifications.h>

#import <React/RCTBridge.h>
#import <React/RCTConvert.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTUtils.h>
#import <Appear2mee/Appear2mee.h>

NSString *const kAppear2meeResponseDismiss = @"appear2meeResponseDismiss";
NSString *const kAppear2meeResponseAccept = @"appear2meeResponseAccept";

static NSDictionary *RCTFormatLocalNotification(UILocalNotification *notification)
{
  NSMutableDictionary *formattedLocalNotification = [NSMutableDictionary dictionary];
  if (notification.fireDate) {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"];
    NSString *fireDateString = [formatter stringFromDate:notification.fireDate];
    formattedLocalNotification[@"fireDate"] = fireDateString;
  }
  formattedLocalNotification[@"alertAction"] = RCTNullIfNil(notification.alertAction);
  formattedLocalNotification[@"alertBody"] = RCTNullIfNil(notification.alertBody);
  formattedLocalNotification[@"applicationIconBadgeNumber"] = @(notification.applicationIconBadgeNumber);
  formattedLocalNotification[@"category"] = RCTNullIfNil(notification.category);
  formattedLocalNotification[@"soundName"] = RCTNullIfNil(notification.soundName);
  formattedLocalNotification[@"userInfo"] = RCTNullIfNil(RCTJSONClean(notification.userInfo));
  formattedLocalNotification[@"remote"] = @NO;
  return formattedLocalNotification;
}

static NSDictionary *RCTFormatUNNotification(UNNotification *notification)
{
  NSMutableDictionary *formattedNotification = [NSMutableDictionary dictionary];
  UNNotificationContent *content = notification.request.content;

  formattedNotification[@"identifier"] = notification.request.identifier;

  if (notification.date) {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"];
    NSString *dateString = [formatter stringFromDate:notification.date];
    formattedNotification[@"date"] = dateString;
  }

  formattedNotification[@"title"] = RCTNullIfNil(content.title);
  formattedNotification[@"body"] = RCTNullIfNil(content.body);
  formattedNotification[@"category"] = RCTNullIfNil(content.categoryIdentifier);
  formattedNotification[@"thread-id"] = RCTNullIfNil(content.threadIdentifier);
  formattedNotification[@"userInfo"] = RCTNullIfNil(RCTJSONClean(content.userInfo));

  return formattedNotification;
}

static NSDictionary *RCTFormatUNNotificationResponse(UNNotificationResponse *response)
{
  UNNotification *notification = response.notification;
  NSMutableDictionary *formattedResponse = (NSMutableDictionary *) RCTFormatUNNotification(notification);
  formattedResponse[@"actionIdentifier"] = RCTNullIfNil(response.actionIdentifier);
  return formattedResponse;
}

@implementation RNAppear2mee
RCT_EXPORT_MODULE();




- (NSArray<NSString *> *)supportedEvents
{
    return @[kAppear2meeResponseDismiss,kAppear2meeResponseAccept];
}

// These notifications come from the Appear2meePushNotificationManager.
// They are managed in this module because they are Appear2mee events.
- (void)startObserving
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAppear2meeAccept:)
                                                 name:kAppear2meeResponseAccept
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAppear2meeDismiss:)
                                                 name:kAppear2meeResponseDismiss
                                               object:nil];
}

- (void)handleAppear2meeAccept:(NSNotification *)notification
{
    [self sendEventWithName:kAppear2meeResponseAccept body:notification.userInfo];
}

- (void)handleAppear2meeDismiss:(NSNotification *)notification
{
    [self sendEventWithName:kAppear2meeResponseDismiss body:notification.userInfo];
}


- (void)stopObserving
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

RCT_EXPORT_METHOD(hasDownloadPermission:(RCTResponseSenderBlock)callback) {
    BOOL permission = [Appear2mee hasDownloadPermission];
    callback(@[[NSNumber numberWithBool:permission]]);
}

RCT_EXPORT_METHOD(hasWifiOnlyPermission:(RCTResponseSenderBlock)callback) {
    BOOL permission = [Appear2mee hasWifiOnlyPermission];
    callback(@[[NSNumber numberWithBool:permission]]);
}

RCT_EXPORT_METHOD(hasServicePermission:(RCTResponseSenderBlock)callback) {
    BOOL permission = [Appear2mee hasServicePermission];
    callback(@[[NSNumber numberWithBool:permission]]);
}

RCT_EXPORT_METHOD(hasSoundOnMutePermission:(RCTResponseSenderBlock)callback) {
    BOOL permission = [Appear2mee hasSoundOnMutePermission];
    callback(@[[NSNumber numberWithBool:permission]]);
}

RCT_EXPORT_METHOD(soundOnMutePermission:(BOOL) value) {
    [Appear2mee soundOnMutePermission:value];
}

RCT_EXPORT_METHOD(servicePermission:(BOOL) value) {
    [Appear2mee servicePermission:value];
}

RCT_EXPORT_METHOD(wifiOnlyPermission:(BOOL) value) {
    [Appear2mee wifiOnlyPermission:value];
}

RCT_EXPORT_METHOD(downloadPermission:(BOOL) value) {
    [Appear2mee downloadPermission:value];
}

RCT_EXPORT_METHOD(registerUserID:(NSString *) value) {
    [Appear2mee registerUserID:value];
}

RCT_EXPORT_METHOD(registerTags:(NSDictionary *) value) {
    [Appear2mee registerTags:value];
}

- (NSNumber *)intFromHexString:(NSString *) hexStr
{
    unsigned int hexInt = 0;

    // Create scanner
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];

    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];

    // Scan hex value
    [scanner scanHexInt:&hexInt];

    return [NSNumber numberWithUnsignedInt:hexInt];
}

RCT_EXPORT_METHOD(configure:(NSDictionary *) _defaults) {
    if(!_defaults) return;
    NSMutableDictionary *defaults = [_defaults mutableCopy];

    NSArray<const NSString *> *colorItems = @[kAppear2meeDisplayBackgroundColor,
                                              kAppear2meeFaceCenterBackgroundColor,
                                              kAppear2meeFaceDisplayBackgroundColor];
    for(NSString *item in colorItems) {
        NSString *col = defaults[item];
        if(col) { defaults[item] = [self intFromHexString:col];}
    }
    [Appear2mee defaultSettings:defaults];
}

RCT_EXPORT_METHOD(tags:(RCTResponseSenderBlock)callback) {
    NSDictionary *tags = [Appear2mee tags];
    callback(@[tags]);
}

+ (BOOL)requiresMainQueueSetup
{
    return YES;  // only do this if your module initialization relies on calling UIKit!
}

- (NSDictionary *)constantsToExport
{
    return @{
             @"notificationDismiss" : kAppear2meeNotificationDismiss,
             // ios 12 will use custom Action.
             @"notificationAccept" : kAppear2meeNotificationAccept,
             // ios 12 will use touch image/video to play.
             @"notificationPlay" : kAppear2meeNotificationPlay,

             @"watchNotificationViewInApp": kAppear2meeWatchNotificationViewInApp,

             // Position of facee in app.
             @"faceHeadPosition" : kAppear2meeFaceHeadPosition,
             @"faceHeadTopRight" : kAppear2meeFaceHeadTopRight,
             @"faceHeadTopLeft" : kAppear2meeFaceHeadTopLeft,
             @"faceHeadBottomRight" : kAppear2meeFaceHeadBottomRight,
             @"faceHeadBottomLeft" : kAppear2meeFaceHeadBottomLeft,
             @"faceHeadCenter" : kAppear2meeFaceHeadCenter,
             @"faceShouldersPosition" : kAppear2meeFaceShouldersPosition,
             @"faceShouldersRight" : kAppear2meeFaceShouldersRight,
             @"faceShouldersLeft" : kAppear2meeFaceShouldersLeft,
             @"faceShouldersCenter" : kAppear2meeFaceShouldersCenter,
             @"displayBackgroundColor" : kAppear2meeDisplayBackgroundColor,
             @"faceDisplayBackgroundColor" : kAppear2meeFaceDisplayBackgroundColor,
             @"faceCenterBackgroundColor" : kAppear2meeFaceCenterBackgroundColor,
             @"faceNotificationSound" :kAppear2meeFaceNotificationSound,
             @"notificationSound" :kAppear2meeNotificationSound,
             @"openExternalURLAutomatically" : kOpenExternalURLAutomatically,
             @"faceContentExtensionBackgroundImage" :kAppear2meeFaceContentExtensionBackgroundImage,
             @"contentExtensionBackgroundImage" :kAppear2meeContentExtensionBackgroundImage,
             @"notificationDismissed":kAppear2meeResponseDismiss,
             @"notificationAccepted":kAppear2meeResponseAccept,
             };
}

#pragma mark Appear2mee Access methods

+(void) initWithID:(NSString *) appear2meeID clientKey:(NSString *) secretKey  usingAppGroup:(NSString *) appGroup  {
    [Appear2mee initWithID:appear2meeID clientKey:secretKey usingAppGroup:appGroup];
}


+ (NSString * _Nonnull)registerDeviceToken:(NSData* _Nonnull)deviceToken {
    return [Appear2mee registerDeviceToken:deviceToken];
}

+(void) addAppear2meeNotificationCategory {
    [Appear2mee addAppear2meeNotificationCategory];
}

+(BOOL) didReceiveNotificationResponse:(UNNotificationResponse *_Nonnull)response withAcceptAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) acceptAction withDismissAction:(void(^_Nonnull)(NSDictionary *_Nonnull)) dismissAction  API_AVAILABLE(ios(10.0)){
    return [Appear2mee didReceiveNotificationResponse:response withAcceptAction:acceptAction withDismissAction:dismissAction];
}

+(BOOL) didReceiveRemoteNotification:(NSDictionary * _Nonnull) payload fetchCompletionHandler:(void (^_Nullable)(UIBackgroundFetchResult result))completionHandler {
   return [Appear2mee didReceiveRemoteNotification:payload fetchCompletionHandler:completionHandler];
}


#pragma mark testing

-(NSString *) nowTimePlus:(double) secs {
    NSDate *now = [NSDate date];
    NSDate* newDate = [now dateByAddingTimeInterval:secs];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString *dateString = [dateFormat stringFromDate:newDate];
    return dateString;
}

static int msgCount = 0;

RCT_EXPORT_METHOD(testFace) {
    NSString *playDate = [self nowTimePlus:10.0]; // one min ago.
    static BOOL toggle=false;
    toggle= toggle?false:true;

    NSString *jobid;

    NSString *url,*about,*action;
    if(toggle) {
        url=@"https://faceestock.s3.eu-geo.objectstorage.softlayer.net/c0ccde25-376c-4d18-a519-83dc851b88a5.mp4";
        about=@"[{\"subject\":\"Not Funny\"},{\"page\":\"Not Funny\"},{\"link\":\"Not Funny\"}]";
        jobid = @"a60730a9555731f257f3b00f09890485_2ee";
        action = @"Click Me";
    } else {
        url=@"https://faceestock.s3.eu-geo.objectstorage.softlayer.net/64472632-f79f-4299-8577-55cf4f7bc53b.mp4";
        about=@"[{\"subject\":\"Box\"},{\"page\":\"Party\"},{\"link\":\"Birthday\"}]";
        jobid = @"1e19a1f0c2105be90d4fc75b7303ade3_cro";
        action = @"Party";
    }

    msgCount++;

    NSDictionary *payload =
    @{@"payload":@{@"fm":@{@"action":action,
                           @"about":about,
                           @"title": [NSString stringWithFormat:@"A Title %d", msgCount],
                           @"subtitle":@"More Title Information",
                           @"url":url,
                           @"jobId":jobid,
                           @"message":@"One more test message",@"playTime":playDate,@"expiryTime":@120}
                   }
      };

    [Appear2mee testNotification:payload];
}


RCT_EXPORT_METHOD(testRichText) {

    NSString *playDate = [self nowTimePlus:-60.0]; // one min ago.
    static BOOL toggle=false;
    toggle= !toggle;
    static BOOL toggle2=false;

    NSString *jobid;

    NSString *path,*key,*about,*action;
    if(toggle) {
        toggle2= !toggle2;
        if(toggle2) {
            key = @"img";
            path = @"https://media.wired.com/photos/5b45021f3808c83da3503cc7/master/w_600,c_limit/tumblr_inline_mjx5ioXh8l1qz4rgp.gif";
            about=@"[{\"subject\":\"Not Funny\"},{\"page\":\"Not Funny\"},{\"link\":\"Not Funny\"}]";
            jobid = @"richa60730a9555731f257f3b00f09890485_2ee";
            action = @"Hear a Joke";
        } else {
            key = @"img";
            path=@"https://www.freeiconspng.com/uploads/png-file-type-icon-0.png";
            about=@"[{\"subject\":\"Funny\"},{\"page\":\"Not Funny\"},{\"link\":\"Funny\"}]";
            jobid = @"rich1e19a1f0c2105be90d4fc75b7303ade3_cro";
            action = @"Have a bet";
        }
    } else {
        if(toggle2) {
            key = @"vid";
            path = @"https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4";
            about=@"[{\"subject\":\"Not Funny\"},{\"page\":\"Not Funny\"},{\"link\":\"Not Funny\"}]";
            jobid = @"richa60730a9555731f257f3b00f09890485_2ee";
            action = @"Hear a Joke";
        } else {
            key = @"aud";
            path=@"https://sample-videos.com/audio/mp3/crowd-cheering.mp3";
            about=@"[{\"subject\":\"Funny\"},{\"page\":\"Not Funny\"},{\"link\":\"Funny\"}]";
            jobid = @"rich1e19a1f0c2105be90d4fc75b7303ade3_cro";
            action = @"Have a bet";
        }
    }

    msgCount++;
    NSDictionary *payload =
    @{@"payload":@{@"fm":@{@"action":action,
                           @"about":about,
                           @"title": [NSString stringWithFormat:@"A Title %d", msgCount],
                           @"subtitle":@"More Title Information",
                           key:path,
                           @"jobId":jobid,
                           @"message":@"One more test message",@"playTime":playDate,@"expiryTime":@120}
                   }
      };

    [Appear2mee testNotification:payload];
}


+(BOOL) isAppear2meeNotificationResponse:(UNNotificationResponse *_Nullable)response {
  return [Appear2mee isAppear2meeNotificationResponse:response];
}

+ (BOOL) isAppear2meeNotification:(UNNotification *_Nullable)notification {
  return [Appear2mee isAppear2meeNotification:notification];
}



+ (BOOL) isAppear2meeUserInfo:(NSDictionary *_Nullable) userInfo {
  return [Appear2mee isAppear2meeUserInfo:userInfo];
}

+(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler {
  if([RNAppear2mee didReceiveNotificationResponse:response withAcceptAction:^(NSDictionary *exchangeKeyValues) {
    NSLog(@"Accept action");
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppear2meeResponseAccept
                                                        object:self
                                                      userInfo:exchangeKeyValues];

  } withDismissAction:^(NSDictionary *exchangeKeyValues) {
    NSLog(@"Dismiss action");
    [[NSNotificationCenter defaultCenter] postNotificationName:kAppear2meeResponseDismiss
                                                        object:self
                                                      userInfo:exchangeKeyValues];
  }])
  {
    // it was a 2mee Exchange notification
  } else {

  }
  completionHandler();
}

+ (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
  NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
  token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
  NSLog(@"deviceToken %@", token);
  [RNAppear2mee registerDeviceToken:deviceToken];

}


@end
