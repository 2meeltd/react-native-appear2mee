//
//  FMView.h
//
//  Created by Gerard Allan on 09/10/2015.
//  Copyright © 2015 2mee. All rights reserved.
//

#import <CoreMedia/CoreMedia.h>
@class FMView;
@class UNNotificationAttachment;

/**
 *  protocol required for delegates of <FMView>
 */

typedef NS_ENUM(NSInteger, FMType) {eFMTypeError, eFMAvatar, eFMEmotion, eFMMessage, eFMCustom};

typedef NS_ENUM(NSInteger, headTypes) {eFMHead=0,eFMHeadWithNeck=1,eFMShoulders=2,eFMTorso=3};

@protocol FMViewDelegate <NSObject>
@optional
/**
 *  Finished playing a Face Message
 *
 *  @param fm The FMView calling the delegate
 */
-(void) FMFinishedPlay:(FMView * _Nonnull) fm;

/**
 *  Finished playing an animation
 *
 *  @param fm The FMView calling the delegate
 */
-(void) FMFinishedAnimation:(FMView * _Nonnull) fm;

/**
 *  Will start to play an FM
 *
 *  @param fm The FMView calling the delegate
 */
-(void) FMWillPlay:(FMView * _Nonnull) fm;

/**
 *  Ready to play the Face Message
 *
 *  @param fm The FMView calling the delegate
 */
-(void) FMReady:(FMView * _Nonnull) fm;

/**
 *  Face Message Error
 *
 *  @param fm The FMView calling the delegate
 *  @param error the reason for the fail.
 */
-(void) FMFailed:(FMView * _Nonnull) fm withError:(NSError * _Nullable) error;
@end

/**
 * The FMView displays the static image associated with a Face Message, can play the Face Message, and provides an animation, used as part of the deletion process.
 *
 * FMView is a subclass of UIView that displays and plays a Face Message. This class can be used within XIB files or created programatically.
 * After initialisation the FMView can display a Face Message from a local source using `<displayLocalFile:>`.
 *
 * The FMView uses a <FMViewDelegate>, usually in a UIViewController, to communicate changes in state.
 */
@interface FMView : UIView

/**
 Possible sizes (or quality) of Face Message to display static image.
 */
typedef enum FMSize {eFMSizeNative = 0, eFMSizeSmall=1, eFMSizeMedium=2, eFMSizeLarge=3} FMSize;


/**
 *  Default is the video size eFMNative.
 */
@property (readwrite, nonatomic) FMSize sizeFM;

/**
 Fit the video to the view (even if small/medium/large)
 else uses the FMSize.
 default true
 */
@property (readwrite, nonatomic) BOOL fitToView;

/**
 *  Is Face Message Playing.
 */
@property (readonly, nonatomic) BOOL isPlaying;
// Is loading.

/**
 *  Is Face Message loading.
 */
@property (readonly, nonatomic) BOOL isLoading;

/**
 *  Is busy ... playing loading.
 */
@property (readonly, nonatomic) BOOL isBusy;


/**
 *  Face Message should only be played after this time. If there is no restriction the property will be nil.
 */
@property (readonly, nonatomic) NSDate * _Nullable fmPlayDate;


/**
 *  Face Message expiry date.
 */
@property (readonly, nonatomic)  NSDate * _Nullable fmExpiryDate;

/**
 *  Face Message type (eFMAvatar or eFMMessage).
 */
@property (readonly, nonatomic)  FMType fmType;

/**
 *  Number of plays permitted. Usually 1 or infinite. If limited number of plays permitted, third party apps must respect this and not reload data to re-play beyond limit.
 */
@property (readonly, nonatomic) unsigned int fmPermittedPlays;

/**
 *  True if play is permitted. Play might not be permitted because of a number of reasons, the Face Message  may have expired, the number of permitted plays may have been reach etc.
 *
 *  Note: A true value does not mean that the FMView <canPlay>. This property relates only to the FMView permissions.
 */
@property (readonly, nonatomic) BOOL fmPlayPermitted;


/**
 *  True if the Face Message has timed out. If the static image is currently displayed it will be unchanged, but the FMView should be retired. The use of an expired Face Message is undefined. The backing video file may no longer be present.
 */
@property (readonly, nonatomic) BOOL fmHasExpired;

/**
 *  The seek time used to generate the Face Message static image view.
 */
@property (readonly, nonatomic) float fmStaticImageSeekTime;

/**
 *  The duration of the Face Mesasage. 0 is no Face Message.
 */
@property (readonly, nonatomic) float fmDuration;

/**
 *  the Face Message delegate, receives FMFinished etc.
 */
@property (weak, nonatomic) id<FMViewDelegate> _Nullable delegate;

/**
 *  manual init with position frame.
 *
 *  @param frame UIView property frame
 *
 *  @return FMView
 */
- (instancetype _Nonnull)initWithFrame:(CGRect)frame;


/**
 *  Loads and displays a static image.
 *
 *  @param localfile Face Message to play from a file path.
 *  @return returns true if file exists, else false.
 */
- (BOOL) displayLocalFile:(NSString * _Nonnull) localfile;


/**
 *  Loads and displays a static image, with fadein  animation. When animation finishs, start play.
 *
 *  @param localfile Face Message to play from a file path.
 *  @param secs time of fadein animation..
 *  @return returns true if file exists, else false.
 */
- (BOOL) animatedThenPlayLocalFile:(NSString * _Nonnull) localfile after:(NSTimeInterval) secs;

/**
 *  Loads and start to play immediately. It is recommended to use  displayLocalFile: and allow user to start play.
 *
 *  @param localfile The Face Message to play from a file path.
 *  @return returns true if file exists, else false.
 */
- (BOOL) playLocalFile:(NSString * _Nonnull) localfile;


/**
 *  Loads the static image. Only usable with the correctly formed userInfo dictionary from a 2Mee exchange push notification
 *  Requires that Bridge2Mee has been initialized with a sharegroup.
 
 *  @param userInfo A userInfo dictionary in a content extension. Play the Face Message given in the userInfo dictionary.
 *
 */
-(void) displayExternal:(NSDictionary *) userInfo;

/**
 *  Loads and start to play immediately. Only usable with the correctly formed userInfo dictionary from a 2Mee exchange push notification
 *  Requires that Bridge2Mee has been initialized with a sharegroup.
 
 *  @param userInfo A userInfo dictionary in a content extension. Play the Face Message given in the userInfo dictionary.
 *
 */
-(void) playExternal:(NSDictionary *) userInfo;

/**
 *  @return true if a new play can start. If already playing or in animation sequence returns false
 */
-(BOOL) canPlay;

/**
 *  Play Face Message
 *
 *  @return Will returns true if playing started else if can not play returns false.
 *  False could mean playing already,animating,nothing to play etc..
 */
- (BOOL) play;


/**
 *  Halt Face Message
 *
 *  Halt or Stops the Face Message which is currently playing.
 *
 */
- (void) haltPlay;


/**
 *  Try to start animation
 *
 *  @return If animation starts return true. If playing or in animation sequence returns false.
 */
-(BOOL) shakeAndExplode;

/**
 *  Cancel animation
 *
 *  @return true if canceled.  Can cancel up to time explosion happens. If already exploded is to late to cancel!
 */
-(BOOL) cancelShakeAndExplode;


/**
 *  Clear all data and reset views to nil.
 */
-(void) clear;


/**
 *  Return true if a local fm at fmURL has expired.
 *
 *
 */
+(BOOL) hasExpired: (NSURL *_Nonnull) fmURL;


/**
 *  Return true if a local fm at fmURL has expired plus the grace period.
 *
 *  @param fileURL  The Face Message from a file path.
 *  @param interval The interval in seconds (can be negative)
 */
+(BOOL) hasExpired: (NSURL *) fileURL withGracePeriod:(NSTimeInterval) interval;


/**
 *  Return an Image from the Face Massage. This image is the place holder image a FMView would display.
 *
 *  @param fileURL  The Face Message from a file path.
 */

+(UIImage *)FMImage: (NSURL *) fileURL;

/**
 *  If set then returns only approvedChats. Default false;
 *  Approved Chats are a feature of Business Face Message.
 *
 *  @param value true - only approved chat will be downloaded. False all chats.
 */
+(void) approvedOnly:(BOOL)value;

/**
 *  approvedOnly status. Default false;
 *  Approved Chats are a feature of Business Face Message.
 *
 *  @return approved only status
 */
+(BOOL) approvedOnly;

/**
 *  Static function to call get the library to link. If there are no references in the code to the view, for example only using via XIB files.
 */
+(id _Nonnull) ensureIamLinked;

/**
 *  Helper function: Returns the headtype number based on the fileurl.
 *
 *  @return headtype number
 */

-(enum headTypes) headType: (NSString * _Nonnull) fileURL;


@property (nonatomic) CMTimeRange range;
@property (nonatomic) CMTime durationChangeRange;
-(BOOL) playRange:(CMTimeRange) range;
-(CMTime) currentTime;
// static image is not used. use zero or range.start
@property (readwrite, nonatomic) BOOL seeker;
+(UIImage *_Nonnull) makeAlphaImageFrom5050:(UIImage *_Nonnull) inputImage;
@end



