
Pod::Spec.new do |s|
  s.name         = "RNAppear2mee"
  s.version      = "1.1.4"
  s.summary      = "Appear2mee notifications"
  s.description  = <<-DESC
                  RNAppear2mee
                   DESC
  s.homepage     = "https://2mee.com/"
  s.license      = "Copyright 2mee Ltd"
  # s.license      = { :type => "2meeLtd", :file => "FILE_LICENSE" }
  s.author             = { "author" => "2mee@2mee.com" }
  s.platforms    = { :ios => "9.0", :tvos => "9.0" }
  s.source       = { :bitbucket => "https://bitbucket.com/2meeltd/react-native-appear2mee.git", :tag => "master" }
  s.source_files  = "ios/**/*.{h,m}"
  s.requires_arc = true
 # s.preserve_paths = 'ios/Appear2meeFramework/Appear2mee.framework'
 # s.xcconfig = { 'OTHER_LDFLAGS' => '-framework Appear2mee' }
  s.vendored_frameworks = 'Appear2meeFramework/Appear2mee.xcframework'
  s.dependency "React"
  #s.dependency "others"

end
