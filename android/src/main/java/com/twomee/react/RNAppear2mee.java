
package com.twomee.react;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;


import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.facebook.react.ReactApplication;
import com.facebook.react.ReactInstanceManager;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.WritableNativeMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.google.firebase.messaging.RemoteMessage;
import com.twomee.appear2mee.Appear2mee;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

class Appear2meeListener implements Appear2mee.ResponseListenerCallback
{
  // static variable single_instance of type Singleton
  private static Appear2meeListener  single_instance = null;

  // variable of type String
  public String s;
  public ReactApplication application;

  // private constructor restricted to this class itself
  private Appear2meeListener ()
  {
    s = "Hello I am a string part of Singleton class";
  }

  // static method to create instance of Singleton class
  public static Appear2meeListener  getInstance()
  {
    if (single_instance == null)
      single_instance = new Appear2meeListener ();

    return single_instance;
  }

  @Override
  public Appear2mee.ResponseListener appear2meeResponseListener(Bundle payload) {

    return new Appear2mee.ResponseListener() {
      @Override
      public void acceptAction(Map<String, String> keyValueDict) {
        RNAppear2mee.notificationAccepted(application,keyValueDict);
        //Toast.makeText(MainApplication.this , String.format("key/values %s",keyValueDict.toString()), Toast.LENGTH_LONG).show();
      }

      @Override
      public void dismissAction(Map<String, String> keyValueDict) {
        RNAppear2mee.notificationDismissed(application,keyValueDict);
        //Toast.makeText(MainApplication.this , "Dismissed", Toast.LENGTH_LONG).show();
      }
    };
  }
}

public class RNAppear2mee extends ReactContextBaseJavaModule {
  public static final String LOG_TAG = "RNAppear2mee";// all logging should use this tag
  public static final String kAppear2meeResponseDismiss = "appear2meeResponseDismiss";
  public static final String kAppear2meeResponseAccept = "appear2meeResponseAccept";

  public RNAppear2mee(ReactApplicationContext reactContext) {
    super(reactContext);
    Application applicationContext = (Application) reactContext.getApplicationContext();
  }

  @Override
  public String getName() {
    return "RNAppear2mee";
  }

  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put("notificationDismiss",Appear2mee.kNotificationDismiss);
    constants.put("notificationAccept",Appear2mee.kNotificationAccept);
    // ios 12 will use touch image/video to play.
    constants.put("notificationPlay","notused");
    // constants.put("watchNotificationViewInApp",Appear2mee.kWatchNotificationViewInApp);

    // Position of facee in app.
    constants.put("faceHeadPosition", Appear2mee.kFaceHeadPosition);
    constants.put("faceHeadTopRight" , Appear2mee.kFaceHeadTopRight);
    constants.put("faceHeadTopLeft" , Appear2mee.kFaceHeadTopLeft);
    constants.put("faceHeadBottomRight" , Appear2mee.kFaceHeadBottomRight);
    constants.put("faceHeadBottomLeft" , Appear2mee.kFaceHeadBottomLeft);
    constants.put("faceHeadCenter" , Appear2mee.kFaceHeadCenter);
    constants.put("faceShouldersPosition" , Appear2mee.kFaceShouldersPosition);
    constants.put("faceShouldersRight" , Appear2mee.kFaceShouldersRight);
    constants.put("faceShouldersLeft" , Appear2mee.kFaceShouldersLeft);
    constants.put("faceShouldersCenter" , Appear2mee.kFaceShouldersCenter);
    constants.put("displayBackgroundColor" , Appear2mee.kDisplayBackgroundColor);
    constants.put("faceDisplayBackgroundColor" , Appear2mee.kFaceDisplayBackgroundColor);
    constants.put("faceCenterBackgroundColor" , Appear2mee.kFaceCenterBackgroundColor);
    constants.put("faceNotificationSound" , Appear2mee.kFaceNotificationSound);
    constants.put("notificationSound",Appear2mee.kNotificationSound);
    constants.put("openExternalURLAutomatically" , Appear2mee.kOpenExternalURLAutomatically);
    constants.put("faceContentExtensionBackgroundImage" ,"notused");
    constants.put("contentExtensionBackgroundImage" ,"notused");

    // response to notification event names
    constants.put("notificationDismissed",kAppear2meeResponseDismiss);
    constants.put("notificationAccepted",kAppear2meeResponseAccept);
    return constants;
  }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
            .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
            .emit(eventName, params);
  }


  @ReactMethod
  public void hasWifiOnlyPermission(Callback callback) {
    Boolean res = Appear2mee.hasWifiOnlyPermission();
    callback.invoke(res);
  }

  @ReactMethod
  public void hasDownloadPermission(Callback callback) {
    Boolean res = Appear2mee.hasDownloadPermission();
    callback.invoke(res);
  }

  @ReactMethod
  public void hasSoundOnMutePermission(Callback callback) {
    callback.invoke(false); // no sound on mute in android
  }

  @ReactMethod
  public void hasServicePermission(Callback callback) {
    Boolean res = Appear2mee.hasServicePermission();
    callback.invoke(res);
  }

  @ReactMethod
  public void wifiOnlyPermission(Boolean set) {
    Appear2mee.WifiOnlyPermission(set);
  }

  @ReactMethod
  public void servicePermission(Boolean set) {
    Appear2mee.servicePermission(set);
  }

  @ReactMethod
  public void soundOnMutePermission(Boolean set) {
    // do nothing in android
  }

  @ReactMethod
  public void downloadPermission(Boolean set) {
    Appear2mee.downloadPermission(set);
  }

  @ReactMethod
  public void registerUserID(String value) {
    Appear2mee.registerUserID(value);
  }

  @ReactMethod
  public void registerTags(ReadableMap value) {
    Map<String,String> tags  = new HashMap<String,String>();
    for (ReadableMapKeySetIterator it = value.keySetIterator();
         it.hasNextKey();
    ) {
      String key = it.nextKey();
      ReadableType type = value.getType(key);
      switch(type) {
        case String:
          tags.put(key,value.getString(key));
          break;
        default:
      }
    }
    Appear2mee.registerTags(tags);
  }

  @ReactMethod
  public void tags(Callback callback) {
    Map<String,String> tags = Appear2mee.getTags();
    WritableMap map = new WritableNativeMap();
    for (Map.Entry<String, String> entry : tags.entrySet()) {
      map.putString(entry.getKey(), entry.getValue());
    }
    callback.invoke(map);
  }

  static final String APPEAR2MEE_ADDRESS = "https://exchangebridge.2mee.com/";
  public static void init(ReactApplication app, String appear2meeID, String  appear2meeKey) {
    Appear2meeListener.getInstance().application = app;
    Appear2mee.reactOnlyinit((Application)app,Appear2meeListener.getInstance(),appear2meeID,appear2meeKey,APPEAR2MEE_ADDRESS);
  }

  public static void notificationAccepted(ReactApplication reactApp, Map<String,String> dict) {
    ReactNativeHost reactNativeHost = reactApp.getReactNativeHost();
    ReactInstanceManager reactInstanceManager = reactNativeHost.getReactInstanceManager();
    ReactContext reactContext = reactInstanceManager.getCurrentReactContext();

    WritableMap params = Arguments.createMap();
    for (Map.Entry<String,String> entry : dict.entrySet()) {
      params.putString(entry.getKey(), entry.getValue());
    }

    reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(kAppear2meeResponseAccept, params);
  }

  public static void notificationDismissed(ReactApplication reactApp, Map<String,String> dict) {
    ReactNativeHost reactNativeHost = reactApp.getReactNativeHost();
    ReactInstanceManager reactInstanceManager = reactNativeHost.getReactInstanceManager();
    ReactContext reactContext = reactInstanceManager.getCurrentReactContext();

    WritableMap params = Arguments.createMap();
    for (Map.Entry<String,String> entry : dict.entrySet()) {
      params.putString(entry.getKey(), entry.getValue());
    }

    reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(kAppear2meeResponseDismiss, params);
  }


  static boolean toggleRich = false;
  static boolean toggleRich2 = false;

  @ReactMethod
  public void testRichText() {
    boolean isVideo = false;
    boolean isAudio = false;
    try {
      String img = null;
      String aud = null;
      String jobId;

      if(toggleRich2) {
        if (toggleRich) {
          img = "https://exchange.cdnedge.bluemix.net/web/flowerPot.png";
          aud = "https://exchange.cdnedge.bluemix.net/web/crowd-cheering.mp3";
          isAudio = true;
          jobId = "pnga60730a9555731f257f3b00f09890485";
        } else {
          img = "https://exchange.cdnedge.bluemix.net/web/big_buck_bunny_720p_1mb.mp4";
          isVideo = true;
          // img   = "https://media.wired.com/photos/5b45021f3808c83da3503cc7/master/w_600,c_limit/tumblr_inline_mjx5ioXh8l1qz4rgp.gif";
          jobId = "gsvgreb1e19a1f0c2105be90d4fc75b7303ade";
        }
      } else {
        if(toggleRich) {
          img = "https://exchange.cdnedge.bluemix.net/web/tumblr_inline_mjx5ioXh8l1qz4rgp.gif";
          jobId = "2gsvgreb1e19a1f0c2105be90d4fc75b7303ade";
        } else {
          jobId = "textvgreb1e19a1f0c2105be90d4fc75b7303ade";
        }
      }

      JSONObject fms = new JSONObject();
      JSONObject fmsViewInformation = new JSONObject();
      fms.put("fm", fmsViewInformation);
      fmsViewInformation.put("jobId", jobId);
      if(isVideo) {
        fmsViewInformation.put("vid", img);
      } else {
        if(img != null) {
          fmsViewInformation.put("img", img);
        }
      }
      if(isAudio) {
        fmsViewInformation.put("aud", aud);
      }

      fmsViewInformation.put("subtitle", "More Title Information");
      fmsViewInformation.put("message", "One more test message");
      fmsViewInformation.put("fbTitle", "FB, A Title");
      fmsViewInformation.put("fbSub", "FB, More Title Information");
      fmsViewInformation.put("fbBody", "FB, One more test message");
      fmsViewInformation.put("action", "Press me too.");
      fmsViewInformation.put("expiryTime", 600);
      fmsViewInformation.put("playTime", getTime(+(1 * 10))); // play in 10 secs.

      if(!toggleRich && !toggleRich2) {
        // build in open link in browser
        // as long as overrideDefaultSettings.put(Appear2mee.kOpenExternalURLAutomatically,"true"); is not set to false.
        fmsViewInformation.put("title", "Open Link");
        fmsViewInformation.put("about", "[ { \"__openExternalURL\":\"https://www.google.com\"}, { \"link2\":\"about2.html\"} ]" );
      } else {
        fmsViewInformation.put("title", "A Title");
        fmsViewInformation.put("about", "[ { \"page\":\"about.html\"}, { \"page\":\"about2.html\"} ]");
      }

      toggleRich = !toggleRich;
      if(toggleRich) toggleRich2 = !toggleRich2;

      RemoteMessage.Builder _notification = new RemoteMessage.Builder("bridge2mee");
      _notification.addData("payload", fms.toString());
      RemoteMessage notificationMessage = _notification.build();
      Appear2mee.testNotification(notificationMessage);
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }


  static boolean toggle = false;

  @ReactMethod
  public void testFace() {
    try {


      String url = "https://exchange.cdnedge.bluemix.net/f912bbcc-f890-47e6-aacb-32663e120f97-78242607-5976-4f4f-8d25-95ce11cbe190-take-1.mp4";
      String jobId = "f912bbcc-f890-47e6-aacb-32663e120f97";

      if(toggle) {
        url = "https://exchange.cdnedge.bluemix.net/dc929fea-7e52-48f0-a5a0-b59044de7788-62b1773a-fec3-45c2-aa08-5a52ac0db636-take-1.mp4";
        jobId = "dc929fea-7e52-48f0-a5a0-b59044de7788";
      }
      toggle = !toggle;

      JSONObject fms = new JSONObject();
      JSONObject fmsViewInformation = new JSONObject();
      fms.put("fm", fmsViewInformation);
      fmsViewInformation.put("jobId", jobId);
      fmsViewInformation.put("url", url);
      fmsViewInformation.put("title", "A Title");
      fmsViewInformation.put("subtitle", "More Title Information");
      fmsViewInformation.put("message", "One more test message");
      fmsViewInformation.put("fbTitle", "FB, A Title");
      fmsViewInformation.put("fbSub", "FB, More Title Information");
      fmsViewInformation.put("fbBody", "FB, One more test message");
      fmsViewInformation.put("about", "[ { \"page\":\"about.html\"}, { \"page\":\"about2.html\"}, { \"setColor\":\"true\"} ]" );
      fmsViewInformation.put("expiryTime", 300);
      fmsViewInformation.put("action", "Press Me");
      fmsViewInformation.put("playTime", getTime(-(1 * 20))); // play now!
      RemoteMessage.Builder _notification = new RemoteMessage.Builder("bridge2mee");
      _notification.addData("payload", fms.toString());
      RemoteMessage notificationMessage = _notification.build();
      Appear2mee.testNotification(notificationMessage);
    } catch (JSONException e) {
      e.printStackTrace();
    }
  }

  String getTime(int time) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    Date _currentDate = new Date();
    _currentDate.setTime(_currentDate.getTime() + (time * 1000));
    return dateFormat.format(_currentDate);
  }

  // removed @Override temporarily just to get it working on different versions of RN
  public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    onActivityResult(requestCode, resultCode, data);
  }

  // removed @Override temporarily just to get it working on different versions of RN
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    // Ignored, required to implement ActivityEventListener for RN 0.33
  }

}

